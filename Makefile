.PHONY: all test

all: test

test: kobo-aura-h2o-edition2-qm2.jpg
	xdg-open $<

kobo-aura-h2o-edition2-qm2.jpg: kobo-aura-h2o-edition2-qm2.dot
	dot -T jpg $< > $@
